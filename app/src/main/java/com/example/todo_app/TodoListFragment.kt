package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo_app.ApiClient.Companion.NO_ERROR_CODE
import com.google.android.material.snackbar.Snackbar
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {
    private var isDeleteMode = false
    private lateinit var adapter: TodoListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        CoroutineScope(Dispatchers.IO).launch {
            isDeleteMode = false
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_add -> {
                goToTodoListFragment()
            }
            R.id.menu_delete -> {
                isDeleteMode = !isDeleteMode
                val icon =
                    if (isDeleteMode) R.drawable.ic_check else R.drawable.ic_delete
                item.setIcon(icon)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        recycler.also {
            it.layoutManager = LinearLayoutManager(activity)
            adapter = TodoListAdapter(::onClickTodoItem)
            it.adapter = adapter
            it.addItemDecoration(
                DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            )
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = ApiClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                val body = response.body() ?: throw Exception()
                if (body.errorCode == NO_ERROR_CODE) {
                    fetchTodoList()
                } else {
                    withContext(Dispatchers.Main) {
                        showErrorAlertDialog(body.errorMessage)
                    }
                }
            } else {
                onFailureResponse(response.errorBody()?.string())
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private suspend fun fetchTodoList() {
        try {
            val response = ApiClient().apiRequest.fetchTodos().execute()
            if (response.isSuccessful) {
                val body = response.body() ?: throw Exception()
                withContext(Dispatchers.Main) {
                    adapter.todoList = body.todos!!
                }
            } else {
                onFailureResponse(response.errorBody()?.string())
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private suspend fun onFailureResponse(message: String?) {
        val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
        val body = gson.fromJson(message, BasicResponse::class.java)
        withContext(Dispatchers.Main) {
            showErrorAlertDialog(body.errorMessage)
        }
    }

    private fun onClickTodoItem(todo: Todo) {
        if (isDeleteMode) {
            showDeleteDialog(todo)
        } else {
            goToTodoListFragment(todo)
        }
    }

    private fun goToTodoListFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionTodoEdit(todo)
        findNavController().navigate(action)
    }

    private fun showDeleteDialog(todo: Todo) {
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.message_delete_success, todo.title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Dispatchers.IO).launch {
                    deleteTodo(todo.id)
                }
            }.show()
    }
}
