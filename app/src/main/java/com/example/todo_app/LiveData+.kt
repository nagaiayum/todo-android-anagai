package com.example.todo_app

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/** [Event] が流れて来る度に一度だけ発火する observer */
fun <T> LiveData<Event<T>>.observeEvent(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer {
        val content = it?.getContentIfNotHandled() ?: return@Observer
        observer.invoke(content)
    })
}