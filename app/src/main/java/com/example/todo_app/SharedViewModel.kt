package com.example.todo_app

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    var todoEditResult: MutableLiveData<Event<String>> = MutableLiveData()
}