package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TodoRequestBody(
    val title: String,
    val detail: String?,
    val date: String?
) : Parcelable
