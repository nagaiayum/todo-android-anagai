package com.example.todo_app

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {

    private lateinit var dialog: DatePickerDialog
    private val args by navArgs<TodoEditFragmentArgs>()
    private val format = SimpleDateFormat(FORMAT_DATE, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpEditTexts()
        setUpDatePicker()
        button_register.isEnabled = false
        text_date_content.setOnClickListener {
            dialog.show()
        }
        val todo = args.todo
        todo?.let { setUpTexts(it) }
        setUpButton(todo?.id)
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    private fun setUpEditTexts() {
        edit_text_title.addTextChangedListener {
            text_counter_title_default.text = edit_text_title.length().toString()
            val color =
                if (edit_text_title.length() <= TITLE_MAX_CHARACTER_LIMIT) Color.DKGRAY else Color.RED
            text_counter_title_default.setTextColor(color)
            updateRegisterButton()
        }
        edit_text_detail.addTextChangedListener {
            text_counter_detail_default.text = edit_text_detail.length().toString()
            val color =
                if (edit_text_detail.length() <= DETAIL_MAX_CHARACTER_LIMIT) Color.DKGRAY else Color.RED
            text_counter_detail_default.setTextColor(color)
            updateRegisterButton()
        }
    }

    private fun setUpTexts(todo: Todo) {
        edit_text_title.setText(todo.title)
        todo.detail?.let {
            edit_text_detail.setText(it)
        }
        todo.date?.let {
            text_date_content.text = format.format(it)
        }
    }

    private fun setUpButton(id: Int?) {
        val changeRegButtonText = if (id == null) R.string.btn_text_register else R.string.btn_update
        button_register.text =
            getString(changeRegButtonText)

        button_register.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                sendTodo(id)
            }
        }
    }

    private fun updateRegisterButton() {
        val isTitleTextEnabled = edit_text_title.length() in 1..TITLE_MAX_CHARACTER_LIMIT
        val isDetailTextEnabled = edit_text_detail.length() <= DETAIL_MAX_CHARACTER_LIMIT
        button_register.isEnabled = isTitleTextEnabled && isDetailTextEnabled
    }

    private fun setUpDatePicker() {
        val calendar = Calendar.getInstance()
        dialog = DatePickerDialog(
            requireContext(), { _, y, m, d ->
                text_date_content.text = ("$y/${m + 1}/$d")
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        dialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.btn_date_delete)
        ) { _, _ ->
            text_date_content.text = ""
        }
    }

    private fun makeTodoParameter(): TodoRequestBody {
        val format = SimpleDateFormat(FORMAT_DATE, Locale.getDefault())
        val title = edit_text_title.text.toString()
        val detail = if (edit_text_detail.text.isBlank()) "" else edit_text_detail.text.toString()
        val date = if (text_date_content.text.isBlank()) {
            ""
        } else {
            val parseDate = format.parse(text_date_content.text.toString())
            SimpleDateFormat(FORMAT_SERVER_DATE, Locale.getDefault()).format(parseDate)
        }

        return TodoRequestBody(title, detail, date)
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = ApiClient().apiRequest
            val response =
                if (id == null) {
                    request.createTodo(makeTodoParameter()).execute()
                } else {
                    request.updateTodo(id, makeTodoParameter()).execute()
                }
            if (response.isSuccessful) {
                withContext(Main) {
                    onSuccess(id != null)
                }
            } else {
                val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create()
                val body =
                    gson.fromJson(response.errorBody()?.string(), BasicResponse::class.java)
                withContext(Main) {
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.message_error_exception))
            }
        }
    }

    private fun onSuccess(isUpdateTodo: Boolean) {
        val receiveMessage =
            if (isUpdateTodo) {
                R.string.message_update_success
            } else {
                R.string.message_register_success
            }
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(getString(receiveMessage))
        findNavController().popBackStack()
    }

    companion object {
        private const val TITLE_MAX_CHARACTER_LIMIT = 100
        private const val DETAIL_MAX_CHARACTER_LIMIT = 1000
        private const val FORMAT_DATE = "yyyy/M/d"
        private const val FORMAT_SERVER_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
