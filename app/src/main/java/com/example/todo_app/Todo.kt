package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.Date

@Parcelize
data class Todo(
    val id: Int,
    val title: String,
    val detail: String?,
    val date: Date?
) : Parcelable