package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BasicResponse(
    val errorCode: Int,
    val errorMessage: String
) : Parcelable