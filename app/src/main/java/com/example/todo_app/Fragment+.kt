package com.example.todo_app

import android.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.showErrorAlertDialog(errorMessage: String) {
    AlertDialog.Builder(requireContext())
        .setTitle(R.string.title_error_dialog)
        .setMessage(errorMessage)
        .setPositiveButton(R.string.btn_close, null)
        .show()
}