package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TodosGetResponse(
    val todos: List<Todo>?,
    val errorCode: Int,
    val errorMessage: String
) : Parcelable