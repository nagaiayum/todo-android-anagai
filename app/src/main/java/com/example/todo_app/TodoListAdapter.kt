package com.example.todo_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_todo_list.view.*

class TodoListAdapter(private val todoItemClickListener: (Todo) -> Unit) :
    RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {

    var todoList: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, ViewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_todo_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val todoListPosition = todoList[position]
        holder.itemView.text_title.text = todoListPosition.title
        holder.itemView.setOnClickListener {
            todoItemClickListener.invoke(todoListPosition)
        }
    }

    override fun getItemCount() = todoList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
